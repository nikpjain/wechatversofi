// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init();

const db= cloud.database();



// 云函数入口函数
exports.main = async (event, context) => {

  let { OPENID, APPID, UNIONID } = cloud.getWXContext()

  const fileID = event.fileId;
  const res = await cloud.downloadFile({
    fileID: fileID,

  })
  const buffer = res.fileContent;
  var jsn= JSON.parse(buffer.toString('utf-8'));

  await db.collection("versodb").add({
    data:jsn
  })

 var insight=jsn.insights;
 
  return {
    status:insight
  }
}
